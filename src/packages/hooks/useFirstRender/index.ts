import { useCallback, useEffect } from 'react';

const useFirstRender = (cb: () => void) => {
	const _cb = useCallback(cb, []);
	useEffect(_cb, [_cb]);
};

export default useFirstRender;
