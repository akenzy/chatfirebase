import { Col, Drawer, Row } from "antd"
import useFirstRender from "../../hooks/useFirstRender"
import { ConversationMessageDBRealTime } from "../../schemas/ conversationMessage.dbRealtime"
import { Message, useChatStore } from "../../store/conversation/chatStore"
import MessageList from "../MessageList"
import UserList from "../UserList"
import { useState } from "react"

// eslint-disable-next-line @typescript-eslint/ban-types

const ChatWindow = () => {
  const [isOpenMessage, setIsOpenMessage] = useState(false)
  const messages = useChatStore(state => state.messages)
  const setMessage = useChatStore(state => state.setMessages)
  const conversationId = useChatStore(state => state.conversation)

  useFirstRender(() => {
    const loadMessagesWithConversation = async () => {
      const conversationMessage = new ConversationMessageDBRealTime()
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const signalMessages = await conversationMessage.find() as Record<string, Message>
      const messages = Object.values(signalMessages).filter(message => message.conversationId === conversationId)
      setMessage(messages)
    }
    loadMessagesWithConversation()
  })

  useFirstRender(() => {
    const conversationMessage = new ConversationMessageDBRealTime()
    conversationMessage.onConversationMessage(
      (message: Message[]) => {
        const messages = Object.values(message).filter(message => message.conversationId === conversationId)
        const messageSorted = messages.sort((a, b) => {
          return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
        })
        setMessage(messageSorted)
      }
    )
  })


  const handleOpenMessage = () => {
    setIsOpenMessage(true)
  }
  const handleCloseMessage = () => {
    setIsOpenMessage(false)
  }
  return (
    <div style={{ position: "relative" }}>
      <Row gutter={[24, 24]}>
        <Col span={24} md={4}>
          <UserList handleOpenMessage={handleOpenMessage} />
        </Col>
        <Col span={24} md={20}>
          <Drawer open={isOpenMessage} closable={false} >
            <MessageList messages={messages} handleCloseMessage={handleCloseMessage} />
          </Drawer>
        </Col>
      </Row>
    </div>
  )
}

export default ChatWindow