import { Avatar, List } from "antd"
import useFirstRender from "../../hooks/useFirstRender"
import { UserDBRealTime } from "../../schemas"
import { useUserStore } from "../../store/user/userStore"

// eslint-disable-next-line @typescript-eslint/ban-types
type UserListProps = {
  handleOpenMessage: () => void
}

const UserList = (props: UserListProps) => {
  const { handleOpenMessage } = props
  const setListUsers = useUserStore(state => state.setListUsers)
  const users = useUserStore(state => state.users)
  useFirstRender(() => {
    const userDB = new UserDBRealTime()
    const loadUsers = async () => {
      const users = await userDB.find() as Record<string, { name: string, phone: string }>
      setListUsers(Object.values(users))
    }
    loadUsers()
  })
  return (
    <div style={{
      height: '100%',
      minHeight: '100vh',
      overflow: 'auto',
      borderRight: '1px solid #f0f0f0',
      padding: '8px'
    }}>
      <List
        itemLayout="horizontal"
        dataSource={users}
        renderItem={(item, index) => (
          <List.Item onClick={handleOpenMessage}>
            <List.Item.Meta
              avatar={<Avatar src={`https://xsgames.co/randomusers/avatar.php?g=pixel&key=${index}`} />}
              title={<span>{item.name}</span>}
            />
          </List.Item>
        )}
      />
    </div>
  )
}

export default UserList