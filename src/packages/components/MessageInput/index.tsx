import { Form, Input } from "antd"
import { ConversationMessageDBRealTime } from "../../schemas/ conversationMessage.dbRealtime"

// eslint-disable-next-line @typescript-eslint/ban-types
type MessageInputProps = {
  conversationId: string
  senderId: string
  receiverId: Array<string>
}

type FieldType = {
  message: string
}

const MessageInput = (props: MessageInputProps) => {
  const [form] = Form.useForm();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  // const refMessageInput = useRef<any>(null)
  const onFinish = async (values: FieldType) => {
    if (!values.message.trim()) return
    const conversationMessage = new ConversationMessageDBRealTime()
    await conversationMessage.create({
      message: values.message.trim(),
      conversationId: props.conversationId,
      senderId: props.senderId,
      receiverId: props.receiverId,
      createdAt: new Date().toISOString(),
      type: 'text'
    }, {
    })

    form.setFields([{ name: 'message', value: '' }])
    // refMessageInput.current!.focus()
  }
  const onFinishFailed = (errorInfo: unknown) => {
    console.log('Failed:', errorInfo)
  }
  return (
    <div className="messageFormWrapper">
      <div style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
      }}>
        <Form
          name="basic"
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          className="w-full"
        >
          <Form.Item<FieldType>
            name="message"
            className="mb-0"
          >
            <Input bordered={false} placeholder="Tin nhắn tới ABC" />
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default MessageInput