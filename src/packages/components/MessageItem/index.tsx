import { Avatar, Col, Row } from "antd"

// eslint-disable-next-line @typescript-eslint/ban-types
type MessageListProps = {
  isSender: boolean;
  message: string;
}

const MessageSenderItem = ({ message }: { message: string }) => {
  const time = '10:35'
  return (
    <Row gutter={[8, 8]} justify={"end"} className="w-full">
      <Col span={20}>
        <Row gutter={[8, 8]}>
          <Col span={24} className="messageSender">
            <div className="messageItemWrapper">
              <div className="messageItem">
                <p>{message}</p>
                <p>{time}</p>
              </div>
            </div>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

const MessageReceiverItem = ({ message }: { message: string }) => {
  const time = '10:35'
  return (
    <Row gutter={[8, 8]} className="w-full" justify={"start"} >
      <Col span={20}>
        <Row gutter={[16, 8]}>
          <Col span={3} sm={2} md={4} lg={1}>
            <Avatar />
          </Col>
          <Col span={21} sm={22} md={20} lg={23}>
            <Row gutter={[8, 8]}>
              <Col span={24} >
                <div className="messageItemWrapper">
                  <div className="messageItem">
                    <p>{message}</p>
                    <p>{time}</p>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

const MessageItem = (props: MessageListProps) => {
  const { isSender, message } = props

  const MessageRender = isSender ? MessageSenderItem : MessageReceiverItem
  return <MessageRender message={message} />
}

export default MessageItem