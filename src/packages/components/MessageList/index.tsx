// eslint-disable-next-line @typescript-eslint/ban-types
type MessageListProps = {
  messages: Array<Message>
  handleCloseMessage: () => void
}

import { LeftOutlined } from '@ant-design/icons';
import { Avatar, List, Space, Typography } from 'antd';
import { useEffect, useMemo } from 'react';
import { Message, useChatStore } from '../../store/conversation/chatStore';
import { useUserStore } from '../../store/user/userStore';
import MessageInput from '../MessageInput';
import MessageItem from '../MessageItem';

const MessageHeader = ({ receiverId, handleCloseMessage }: { receiverId: Array<string>, handleCloseMessage: () => void }) => {
  return (
    <Space style={{ padding: "0 12px" }}>
      <LeftOutlined onClick={handleCloseMessage} />
      <Space>
        <Avatar
          src="https://xsgames.co/randomusers/avatar.php?g=pixel&key=1"
          size="large"
        />
        <Typography.Title level={5}>{receiverId.join(", ")}</Typography.Title>
      </Space>
    </Space>

  )
}

const MessageList = (props: MessageListProps) => {
  const { messages, handleCloseMessage } = props
  // const { refEndView } = useScrollEndView()
  const currentUser = useUserStore(state => state.user!.phone)
  useEffect(() => {
    const documentLastMessage = document.querySelector('.messageList .ant-list-items .ant-list-item:last-child')
    if (documentLastMessage) {
      documentLastMessage.scrollIntoView({ behavior: "smooth" })
    }
    console.log('documentLastMessage', documentLastMessage)
  }, [messages])

  const conversationId = useChatStore(state => state.conversation)
  const members = useChatStore(state => state.members)
  const user = useUserStore(state => state.user)
  const receiverId = useMemo(() => {
    const receiver = members.filter(member => member !== user!.phone)
    return receiver
  }, [])
  return (
    <>
      <List
        className='messageList'
        style={{ height: '100vh', overflow: 'auto' }}
        itemLayout="horizontal"
        dataSource={messages}
        header={<MessageHeader receiverId={receiverId} handleCloseMessage={handleCloseMessage} />}
        renderItem={(item) => {
          const isSender = item.senderId === currentUser
          return (
            <List.Item style={{ padding: 12 }}>
              <MessageItem isSender={isSender} message={item.message} />
            </List.Item>
          )
        }}
        footer={<>
          <MessageInput conversationId={conversationId} senderId={user!.phone} receiverId={receiverId} />
        </>
        }
      />
    </>
  )
}

export default MessageList