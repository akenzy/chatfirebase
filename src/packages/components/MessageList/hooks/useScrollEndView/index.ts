import { useRef } from 'react';

export const useScrollEndView = () => {
	const refEndView = useRef<HTMLDivElement>(null);

	const scrollToEndView = () => {
		if (!refEndView.current) {
			throw new Error('DOM is not ready');
		}
		refEndView.current?.scrollIntoView({ behavior: 'smooth' });
	};

	return { refEndView, scrollToEndView };
};
