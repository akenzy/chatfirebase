// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
	apiKey: 'AIzaSyAOD4nmN5AL_-kBrBgGmFIs2RdTN4RK6dE',
	authDomain: 'lab-fcm-3d748.firebaseapp.com',
	projectId: 'lab-fcm-3d748',
	storageBucket: 'lab-fcm-3d748.appspot.com',
	messagingSenderId: '213591462149',
	appId: '1:213591462149:web:3814a69cdc9ad3f0b4676c',
	measurementId: 'G-DELWTB0S3V',
	databaseURL:
		'https://lab-fcm-3d748-default-rtdb.asia-southeast1.firebasedatabase.app/'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const firebaseApp = app;
