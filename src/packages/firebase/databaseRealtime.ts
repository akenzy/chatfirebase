import { child, get, getDatabase, onValue, ref, set } from 'firebase/database';
import { nanoid } from 'nanoid';
import { firebaseApp } from './firebase';

const dbRealTime = getDatabase(firebaseApp);

abstract class ABSDatabaseRealtime {
	protected abstract writeCollection(
		collection: string,
		data: unknown
	): Promise<unknown>;
	protected abstract readCollection(collection: string): Promise<unknown>;
}

export class DatabaseRealtime extends ABSDatabaseRealtime {
	private readonly _dbRef;
	constructor() {
		console.log('DatabaseRealtime');
		super();
		this._dbRef = ref(dbRealTime);
	}

	async writeCollection(
		collection: string,
		data: unknown,
		option?: Record<string, unknown>
	): Promise<boolean> {
		try {
			const id = option?.id || nanoid();
			await set(ref(dbRealTime, `${collection}/${id}`), data);
			return Promise.resolve(true);
		} catch (error) {
			console.error(error);
			return Promise.resolve(false);
		}
	}
	protected async readCollection(collection: string): Promise<unknown> {
		try {
			const snapshot = await get(child(this._dbRef, collection));
			if (snapshot.exists()) {
				console.log("Let's see what's inside snapshot");
				console.log(snapshot.val());
				return snapshot.val();
			} else {
				console.log('No data available');
				return null;
			}
		} catch (error) {
			console.error(error);
			return null;
		}
	}

	protected async onCollectionChange(
		collection: string,
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		cb: (message: any) => void
	): Promise<void> {
		onValue(child(this._dbRef, collection), snapshot => {
			const data = snapshot.val();
			console.log(data);
			cb(data);
			// updateStarCount(postElement, data);
		});
	}
}
