import { getMessaging, getToken } from 'firebase/messaging';
import { firebaseApp } from './firebase';

// Get registration token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
export const fcmFirebase = getMessaging(firebaseApp);

export const requestNotification = () => {
	console.log('Requesting permission...');
	Notification.requestPermission()
		.then(permission => {
			if (permission === 'granted') {
				console.log('Notification permission granted.');
			}
		})
		.catch(err => {
			console.log('Unable to get permission to notify.', err);
		});
};

export const getTokenFCM = () => {
	console.log('getTokenFCM');
	getToken(fcmFirebase, {
		vapidKey:
			'BLlllj5iMX8oi3uISYvQqEwEgPDG9Ll-FAM6eQ9SvTJEbbd1O8BX0I56Wz4zp0Ab_PzoQc9mfvBBbYckVEmCy0A'
	})
		.then(currentToken => {
			console.log('currentToken: ', currentToken);
			if (currentToken) {
				// Send the token to your server and update the UI if necessary
				// ...
				console.log('currentToken: ', currentToken);
			} else {
				// Show permission request UI
				console.log(
					'No registration token available. Request permission to generate one.'
				);
				// ...
			}
		})
		.catch(err => {
			console.log('An error occurred while retrieving token. ', err);
			// ...
		});
};
