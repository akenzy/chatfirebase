import { DatabaseRealtime } from '../firebase/databaseRealtime';

export class ConversationDBRealTime extends DatabaseRealtime {
	private readonly _COLLECTION: string = 'conversations';
	constructor() {
		console.log('ConversationDBRealTime');
		super();
	}

	create(data: unknown, option?: Record<string, unknown>) {
		return this.writeCollection(this._COLLECTION, data, option);
	}

	findById(id: string) {
		return this.readCollection(`${this._COLLECTION}/${id}`);
	}

	find() {
		return this.readCollection(this._COLLECTION);
	}

	onUserChange() {
		return this.onCollectionChange(this._COLLECTION);
	}
}
