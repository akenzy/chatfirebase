import { DatabaseRealtime } from '../firebase/databaseRealtime';
import { Message } from '../store/conversation/chatStore';

export class ConversationMessageDBRealTime extends DatabaseRealtime {
	private readonly _COLLECTION: string = 'conversations-message';
	constructor() {
		console.log('ConversationDBRealTime');
		super();
	}

	create(data: unknown, option?: Record<string, unknown>) {
		return this.writeCollection(this._COLLECTION, data, option);
	}

	findById(id: string) {
		return this.readCollection(`${this._COLLECTION}/${id}`);
	}

	find() {
		return this.readCollection(this._COLLECTION);
	}

	onConversationMessage(cb: (message: Message[]) => void) {
		return this.onCollectionChange(this._COLLECTION, cb);
	}
}
