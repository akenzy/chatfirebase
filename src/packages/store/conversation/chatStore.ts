import { create } from 'zustand';

export type Message = {
	conversationId: string;
	message: string;
	receiverId: string[];
	senderId: string;
	type: 'text';
	createdAt: string;
};

// Define types for your store's state
type ChatState = {
	conversation: string;
	messages: Array<Message>;
	members: string[];
};

// Define types for your store's actions
type ChatActions = {
	setMessages: (messages: Array<Message>) => void;
	addMessage: (message: Message) => void;
	setMembers: (members: string[]) => void;
};

type ChatStore = ChatState & ChatActions;

export const useChatStore = create<ChatStore>(set => ({
	conversation: 'kti1U8tFxNFFGFkvQ62rV',
	messages: [],
	members: ['0947699623', '0947699624'],
	setMessages: messages => set({ messages }),
	addMessage: message =>
		set(state => ({ messages: [...state.messages, message] })),
	setMembers: members => set({ members })
}));
