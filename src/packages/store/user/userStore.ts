import { create } from 'zustand';
import { persist, createJSONStorage } from 'zustand/middleware';
// Define types for your store's state
type UserState = {
	user: { phone: string; name: string };
	users: { phone: string; name: string }[];
};

// Define types for your store's actions
type UserAction = {
	setUser: (user: { phone: string; name: string }) => void;
	setListUsers: (users: { phone: string; name: string }[]) => void;
};

type UserStore = Partial<UserState> & UserAction;

export const useUserStore = create<UserStore>()(
	persist(
		set => ({
			user: undefined,
			setUser: user => set({ user }),
			setListUsers: users => set({ users })
		}),
		{
			name: 'user-storage', // name of the item in the storage (must be unique)
			storage: createJSONStorage(() => sessionStorage) // (optional) by default, 'localStorage' is used
		}
	)
);
