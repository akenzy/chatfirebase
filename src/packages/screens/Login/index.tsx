import { Button, Form, Input } from 'antd';
import { useOnFinishForm } from './hooks/onFinishForm';
import { LoginFormFieldType } from './types';

const LoginPage = () => {
  const { onFinish } = useOnFinishForm()
  return (
    <div className='w-screen h-screen flex justify-center align-center'>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        style={{ maxWidth: 480, width: '100%' }}
      >
        <Form.Item<LoginFormFieldType>
          label="Nhập số điện thoại"
          name="phone"
          rules={[{ required: true, message: 'Vui lòng nhập số điện thoại' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default LoginPage