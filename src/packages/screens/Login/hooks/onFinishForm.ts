import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { UserDBRealTime } from '../../../schemas';
import { LoginFormFieldType } from '../types';
import { useUserStore } from '../../../store/user/userStore';

export const useOnFinishForm = () => {
	const setUser = useUserStore(state => state.setUser);
	const navigate = useNavigate();
	const navigateToChatWindow = useCallback(() => {
		navigate('/chat-window');
	}, [navigate]);

	const onFinish = useCallback(async (values: LoginFormFieldType) => {
		const userDB = new UserDBRealTime();
		const signalUser = (await userDB.findById(values.phone!)) as {
			phone: string;
			name: string;
		};
		if (signalUser) {
			setUser(signalUser);
			navigateToChatWindow();
		}
	}, []);

	return { onFinish };
};
