import { Button, Form, Input } from 'antd';
import { UserDBRealTime } from '../../schemas/user.dbRealtime';

type FieldType = {
  phone?: string;
  name?: string;
};

const onFinish = async (values: FieldType) => {
  console.log('Success:', values);
  const userDB = new UserDBRealTime()
  const signalUser = await userDB.create(values, {
    id: values.phone
  })
  console.log('signalUser', signalUser)
};

const onFinishFailed = (errorInfo: unknown) => {
  console.log('Failed:', errorInfo);
};


const RegisterPage = () => {
  return (
    <div className='w-screen h-screen flex justify-center align-center'>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item<FieldType>
          label="Nhập số điện thoại"
          name="phone"
          rules={[{ required: true, message: 'Vui lòng nhập số điện thoại' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<FieldType>
          label="Nhập tên"
          name="name"
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Đăng ký
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default RegisterPage