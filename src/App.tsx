import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import './App.css';
import ChatWindowPage from './packages/screens/ChatWindow';
import LoginPage from './packages/screens/Login';
import RegisterPage from './packages/screens/Register';

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div>
        <div>Home</div>
      </div>
    ),
  },
  {
    path: "/login",
    element: (
      <LoginPage />
    ),
  },
  {
    path: "/register",
    element: (
      <RegisterPage />
    ),
  },
  {
    path: "/chat-window",
    element: (
      <ChatWindowPage />
    ),
  },
]);


function App() {
  return (
    <main className='container'>
      <RouterProvider router={router} />
    </main>
  )
}

export default App
